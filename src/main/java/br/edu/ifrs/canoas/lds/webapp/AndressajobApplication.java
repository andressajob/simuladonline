package br.edu.ifrs.canoas.lds.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AndressajobApplication {

	public static void main(String[] args) {
		SpringApplication.run(AndressajobApplication.class, args);
	}
}
