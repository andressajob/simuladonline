package br.edu.ifrs.canoas.lds.webapp.controller;

import br.edu.ifrs.canoas.lds.webapp.config.auth.UserImpl;
import br.edu.ifrs.canoas.lds.webapp.domain.*;
import br.edu.ifrs.canoas.lds.webapp.domain.utils.QuestionsSimulated;
import br.edu.ifrs.canoas.lds.webapp.service.AlternativeService;
import br.edu.ifrs.canoas.lds.webapp.service.HistoricService;
import br.edu.ifrs.canoas.lds.webapp.service.SimulatedService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
@RequestMapping("/simulated/")
public class SimulatedController {

    private final SimulatedService simulatedService;
    private final HistoricService historicService;
    private final AlternativeService alternativeService;
    private List<String> letters = new LinkedList<>();
    private boolean error = false;

    public SimulatedController(SimulatedService simulatedService, HistoricService historicService, AlternativeService alternativeService) {
        this.simulatedService = simulatedService;
        this.historicService = historicService;
        this.alternativeService = alternativeService;
        letters.add(0, "<strong>a.</strong>");
        letters.add(1, "<strong>b.</strong>");
        letters.add(2, "<strong>c.</strong>");
        letters.add(3, "<strong>d.</strong>");
        letters.add(4, "<strong>e.</strong>");
    }

    @GetMapping("form")
    public ModelAndView newSimulated(Model model) {
        QuestionsSimulated simulated = new QuestionsSimulated();
        model.addAttribute("simulated", simulated);
        return new ModelAndView("/simulated/form");
    }

    @GetMapping("list")
    public ModelAndView listSimulateds(Model model, @AuthenticationPrincipal UserImpl activeUser) {
        model.addAttribute("error", error);
        model.addAttribute("simulateds", simulatedService.findAllByUserId(activeUser.getUser().getId()));
        error = false;
        return new ModelAndView("/simulated/list");
    }

    @PostMapping("generate")
    public String generate(@ModelAttribute("simulated") QuestionsSimulated simulated, @AuthenticationPrincipal UserImpl activeUser, Model model) {
        Simulated simulatedSave;
        if (simulated.getAmountComputing() == 0 && simulated.getAmountGeneral() == 0 && simulated.getAmountLegislation() == 0 &&
                simulated.getAmountMathematics() == 0 && simulated.getAmountPortuguese() == 0 && simulated.getAmountSpecifics() == 0) {
            return "redirect:/simulated/form/";
        } else {
            simulatedSave = simulatedService.generateSimulated(simulated, activeUser.getUser());
            model.addAttribute("simulated", simulatedSave);
        }
        if (simulated.isNow() && simulatedSave.getId() != null) {
            return "redirect:/simulated/current/" + simulatedSave.getId();
        } else if (simulatedSave.getId() == null) {
            error = true;
        }
        return "redirect:/simulated/list";
    }


    @GetMapping("current/{id}")
    public ModelAndView currentSimulated(@PathVariable Long id, Model model) {
        Simulated simulated = simulatedService.getOne(id);
        for (Question question : simulated.getQuestions()) {
            Collections.shuffle(question.getAlternatives());
        }
        model.addAttribute("simulated", simulated);
        model.addAttribute("letters", letters);
        model.addAttribute("historic", new Historic());
        model.addAttribute("error", error);
        model.addAttribute("done", simulated.isDone());
        error = false;
        return new ModelAndView("/simulated/simulate");
    }

    @PostMapping("answer")
    public String answerSimulated(@ModelAttribute("simulated") Simulated simulated, @AuthenticationPrincipal UserImpl activeUser, Model model) {
        Alternative alternativeChoosed = new Alternative();
        int count = 0;
        if (simulated.getQuestions() != null) {
            for (int i = 0; i < simulated.getQuestions().size(); i++) {
                if (simulated.getQuestions().get(i).getChoosed() != null)
                    count++;
            }
            if (count == simulated.getQuestions().size() && !simulatedService.getOne(simulated.getId()).isDone()) {
                for (int i = 0; i < simulated.getQuestions().size(); i++) {
                    System.out.println("ALTERNATIVA ESCOLHIDA : " + alternativeChoosed.getId() + "\nQUESTÃO : " + simulated.getQuestions().get(i).getId()
                            + "\nDO SIMULADO : " + simulated.getId());
                    Historic historic = new Historic();
                    alternativeChoosed = alternativeService.getOne(Long.parseLong(simulated.getQuestions().get(i).getChoosed()));
                    historic.setQuestion(simulated.getQuestions().get(i));
                    historic.setAlternative(alternativeChoosed);
                    historic.setSimulated(simulated);
                    historic.setUser(activeUser.getUser());
                    historicService.save(historic);
                }
                simulated = simulatedService.doneSimulated(simulated.getId());
            } else {
                error = true;
                return "redirect:/simulated/current/" + simulated.getId();
            }
        }

        return "redirect:/simulated/review/" + simulated.getId();
    }

    @GetMapping("review/{id}")
    public ModelAndView reviewSimulated(@PathVariable Long id, Model model) {
        Simulated simulated = simulatedService.getOne(id);
        List<Historic> historics = historicService.findAllBySimulatedId(id);
        List<Object> list = simulatedService.countHitsOnSimulated(simulated, historics);
        model.addAttribute("letters", letters);
        model.addAttribute("simulated", simulated);
        model.addAttribute("chooseds", list.get(0));
        model.addAttribute("corrects", list.get(1));
        model.addAttribute("hits", list.get(2));
        return new ModelAndView("/simulated/review");
    }
}


