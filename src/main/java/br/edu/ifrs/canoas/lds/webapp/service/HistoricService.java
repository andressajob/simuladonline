package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Historic;
import br.edu.ifrs.canoas.lds.webapp.repository.HistoricRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class HistoricService {
    private final HistoricRepository historicRepository;
    private final SimulatedService simulatedService;
    private final UserService userService;

    public HistoricService(HistoricRepository historicRepository, SimulatedService simulatedService, UserService userService) {
        this.historicRepository = historicRepository;
        this.simulatedService = simulatedService;
        this.userService = userService;
    }

    public Historic getOne(Long id) {
        if (id != null && historicRepository.existsById(id))
            return historicRepository.getOne(id);
        return null;
    }

    public List<Historic> findAllBySimulatedId(Long id) {
        if (id != null && simulatedService.existsById(id))
            return historicRepository.findAllBySimulatedId(id);
        return null;
    }

    public Historic save(Historic historic) {
        if (historic != null)
            return historicRepository.save(historic);
        return null;
    }

    public List<Historic> countQuestionsDone(Long id) {
        if (id != null && userService.existsById(id))
            return historicRepository.findAllByUserId(id);
        return null;
    }
}
