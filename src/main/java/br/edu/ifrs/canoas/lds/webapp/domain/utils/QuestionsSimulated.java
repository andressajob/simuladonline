package br.edu.ifrs.canoas.lds.webapp.domain.utils;

import br.edu.ifrs.canoas.lds.webapp.domain.Simulated;

public class QuestionsSimulated {
    private int amountPortuguese;
    private int amountMathematics;
    private int amountLegislation;
    private int amountSpecifics;
    private int amountGeneral;
    private int amountComputing;
    private boolean now;
    private Simulated simulated;

    public QuestionsSimulated() {
        now = false;
    }

    public QuestionsSimulated(int amountPortuguese, int amountMathematics, int amountLegislation, int amountSpecifics, int amountGeneral, int amountComputing, boolean now, Simulated simulated) {
        this.amountPortuguese = amountPortuguese;
        this.amountMathematics = amountMathematics;
        this.amountLegislation = amountLegislation;
        this.amountSpecifics = amountSpecifics;
        this.amountGeneral = amountGeneral;
        this.amountComputing = amountComputing;
        this.now = now;
        this.simulated = simulated;
    }

    public Simulated getSimulated() {
        return simulated;
    }

    public void setSimulated(Simulated simulated) {
        this.simulated = simulated;
    }

    public int getAmountPortuguese() {
        return amountPortuguese;
    }

    public void setAmountPortuguese(int amountPortuguese) {
        this.amountPortuguese = amountPortuguese;
    }

    public int getAmountMathematics() {
        return amountMathematics;
    }

    public void setAmountMathematics(int amountMathematics) {
        this.amountMathematics = amountMathematics;
    }

    public int getAmountLegislation() {
        return amountLegislation;
    }

    public void setAmountLegislation(int amountLegislation) {
        this.amountLegislation = amountLegislation;
    }

    public int getAmountSpecifics() {
        return amountSpecifics;
    }

    public void setAmountSpecifics(int amountSpecifics) {
        this.amountSpecifics = amountSpecifics;
    }

    public int getAmountGeneral() {
        return amountGeneral;
    }

    public void setAmountGeneral(int amountGeneral) {
        this.amountGeneral = amountGeneral;
    }

    public int getAmountComputing() {
        return amountComputing;
    }

    public void setAmountComputing(int amountComputing) {
        this.amountComputing = amountComputing;
    }

    public boolean isNow() {
        return now;
    }

    public void setNow(boolean now) {
        this.now = now;
    }
}
