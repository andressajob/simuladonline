package br.edu.ifrs.canoas.lds.webapp.controller;

import br.edu.ifrs.canoas.lds.webapp.config.auth.UserImpl;
import br.edu.ifrs.canoas.lds.webapp.domain.Historic;
import br.edu.ifrs.canoas.lds.webapp.domain.Simulated;
import br.edu.ifrs.canoas.lds.webapp.domain.Role;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.service.HistoricService;
import br.edu.ifrs.canoas.lds.webapp.service.RoleService;
import br.edu.ifrs.canoas.lds.webapp.service.SimulatedService;
import br.edu.ifrs.canoas.lds.webapp.service.UserService;

import javax.validation.Valid;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@Controller
@RequestMapping("/user/")
public class UserController {

    private final UserService userService;
    private final RoleService roleService;
    private final HistoricService historicService;
    private final SimulatedService simulatedService;
    private boolean error = false;
    private boolean invalid = false;

    public UserController(UserService userService, RoleService roleService, HistoricService historicService, SimulatedService simulatedService) {
        this.userService = userService;
        this.roleService = roleService;
        this.historicService = historicService;
        this.simulatedService = simulatedService;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("list")
    public ModelAndView listUsers(Model model) {
        model.addAttribute("users", userService.findAll());
        return new ModelAndView("/user/list");
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("form")
    public ModelAndView newUser(Model model) {
        ModelAndView mav = new ModelAndView("/user/form");
        mav.addObject("user", new User());
        model.addAttribute("action", "new");
        model.addAttribute("error", error);
        model.addAttribute("invalid", invalid);
        model.addAttribute("roles", roleService.findAll());
        model.addAttribute("permission", false);
        error = false;
        invalid = false;
        return mav;
    }

    @GetMapping("create")
    public ModelAndView createUser(Model model) {
        ModelAndView mav = new ModelAndView("/user/form");
        mav.addObject("user", new User());
        model.addAttribute("error", error);
        model.addAttribute("invalid", invalid);
        model.addAttribute("action", "new");
        model.addAttribute("permission", false);
        error = false;
        invalid = false;
        return mav;
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("disable/{id}")
    public String disableUser(@PathVariable Long id) {
        userService.disable(id);
        return "redirect:/user/list";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("enable/{id}")
    public String enableUser(@PathVariable Long id) {
        userService.enable(id);
        return "redirect:/user/list";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("view/{id}")
    public ModelAndView viewUser(Model model, @PathVariable Long id) {
        model.addAttribute("user", userService.getOne(id));
        model.addAttribute("action", "view");
        model.addAttribute("roles", roleService.findAll());
        return new ModelAndView("/user/form");
    }

    @GetMapping("edit/{id}")
    public ModelAndView editUser(Model model, @PathVariable Long id, @AuthenticationPrincipal UserImpl activeUser) {
        boolean permissionError = false;
        User user = new User();
        String roleString = getRoles(activeUser);
        if (!id.equals(activeUser.getUser().getId()) && !roleString.equals("ROLE_ADMIN")){
            permissionError = true;
            model.addAttribute("user", user);
            model.addAttribute("action", "edit");
            model.addAttribute("permission", permissionError);
            model.addAttribute("error", error);
            model.addAttribute("invalid", invalid);
            error = false;
            invalid = false;
        } else {
            user = userService.getOne(id);
            model.addAttribute("user", user);
            model.addAttribute("action", "edit");
            model.addAttribute("roles", roleService.findAll());
            model.addAttribute("permission", permissionError);
            model.addAttribute("error", error);
            model.addAttribute("invalid", invalid);
            error = false;
            invalid = false;
        }
        return new ModelAndView("/user/form");
    }

    @PostMapping("save")
    public String save(@Valid User user, @AuthenticationPrincipal UserImpl activeUser) {
        User userValidate = new User();
        String mav = "";
        String roleString = getRoles(activeUser);
        if (user.getUsername().isEmpty() || user.getPassword().isEmpty() || user.getEmail().isEmpty() ||
                user.getName().isEmpty()) {
            error = true;
            if (user.getId() != null) {
                mav = ("redirect:/user/edit/" + user.getId());
            } else {
                if (roleString.equals("ROLE_ADMIN")) {
                    mav = ("redirect:/user/form");
                } else {
                    mav = ("redirect:/user/create");
                }
            }
        } else {
            userValidate = userService.save(user);
            if (userValidate != null) {
                if (roleString.equals("ROLE_ADMIN")) {
                    mav = ("redirect:/user/list");
                } else {
                    mav = ("redirect:/user/profile");
                }
            } else {
                invalid = true;
                if (user.getId() != null) {
                    mav = ("redirect:/user/edit/" + user.getId());
                } else {
                    if (roleString.equals("ROLE_ADMIN")) {
                        mav = ("redirect:/user/form");
                    } else {
                        mav = ("redirect:/user/create");
                    }
                }
            }
        }
        return mav;
    }

    @GetMapping("profile")
    public ModelAndView viewProfile(@AuthenticationPrincipal UserImpl activeUser, Model model) {
        model.addAttribute("user", userService.getOne(activeUser.getUser().getId()));
        List<Historic> historicList = historicService.countQuestionsDone(activeUser.getUser().getId());
        int questionsEspecificos = userService.questionsDoneByCategory(1L, historicList);
        int questionsGerais = userService.questionsDoneByCategory(2L, historicList);
        int questionsInformatica = userService.questionsDoneByCategory(3L, historicList);
        int questionsLegislacao = userService.questionsDoneByCategory(4L, historicList);
        int questionsMatematica = userService.questionsDoneByCategory(5L, historicList);
        int questionsPortugues = userService.questionsDoneByCategory(6L, historicList);

        int correctEspecificos = userService.questionsCorrectByCategory(1L, historicList);
        int correctGerais = userService.questionsCorrectByCategory(2L, historicList);
        int correctInformatica = userService.questionsCorrectByCategory(3L, historicList);
        int correctLegislacao = userService.questionsCorrectByCategory(4L, historicList);
        int correctMatematica = userService.questionsCorrectByCategory(5L, historicList);
        int correctPortugues = userService.questionsCorrectByCategory(6L, historicList);

        System.out.println("QUESTÕES ESP. FEITAS: " + questionsEspecificos + "\nACERTOS: " + correctEspecificos);
        System.out.println("QUESTÕES GER. FEITAS: " + questionsGerais + "\nACERTOS: " + correctGerais);
        System.out.println("QUESTÕES PORT. FEITAS: " + questionsPortugues + "\nACERTOS: " + correctPortugues);
        System.out.println("QUESTÕES INF. FEITAS: " + questionsInformatica + "\nACERTOS: " + correctInformatica);
        System.out.println("QUESTÕES LEG. FEITAS: " + questionsLegislacao + "\nACERTOS: " + correctLegislacao);
        System.out.println("QUESTÕES MAT. FEITAS: " + questionsMatematica + "\nACERTOS: " + correctMatematica);

        List<Simulated> simulateds = simulatedService.findAllByUserIdAndDoneTrue(activeUser.getUser().getId());
        List<Historic> historics;
        List<Object> hits = new LinkedList<>();
        for (Simulated simulated : simulateds) {
            historics = historicService.findAllBySimulatedId(simulated.getId());
            if (historics.size() > 0) {
                hits.add(simulatedService.countHitsOnSimulated(simulated, historics).get(2));
            } else if (simulated.isDone()) {
                hits.add(0);
            }
        }
        System.out.println("SIMULADOS : " + simulateds.size());
        System.out.println("HITS : " + hits.size());
        model.addAttribute("simulateds", simulateds);

        model.addAttribute("hits", hits);
        model.addAttribute("especificos", questionsEspecificos);
        model.addAttribute("gerais", questionsGerais);
        model.addAttribute("portugues", questionsPortugues);
        model.addAttribute("matematica", questionsMatematica);
        model.addAttribute("legislacao", questionsLegislacao);
        model.addAttribute("informatica", questionsInformatica);
        model.addAttribute("especificosAcertos", correctEspecificos);
        model.addAttribute("geraisAcertos", correctGerais);
        model.addAttribute("portuguesAcertos", correctPortugues);
        model.addAttribute("matematicaAcertos", correctMatematica);
        model.addAttribute("legislacaoAcertos", correctLegislacao);
        model.addAttribute("informaticaAcertos", correctInformatica);
        return new ModelAndView("/user/profile");
    }

    public static String getRoles (UserImpl activeUser){
        String roleString = "";
        if (activeUser != null) {
            Set<Role> roles = activeUser.getUser().getRoles();
            for (Role role : roles) {
                roleString = role.getRole();
            }
        }
        return roleString;
    }
}
