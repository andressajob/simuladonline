package br.edu.ifrs.canoas.lds.webapp.domain;


import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
public class Alternative {

    @Id
    @GeneratedValue
    private Long id;
    @Size(max=2000)
    private String content;
    private boolean correct;
    @ManyToOne
    private Question question;

    public Alternative() {}

    public Alternative(String content, boolean correct, Question question) {
        this.content = content;
        this.correct = correct;
        this.question = question;
    }

    public String getContent() {
        return content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "Alternative{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", correct=" + correct +
                ", question=" + question +
                '}';
    }
}
