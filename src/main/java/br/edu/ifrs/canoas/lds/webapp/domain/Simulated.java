package br.edu.ifrs.canoas.lds.webapp.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Simulated {
    @Id
    @GeneratedValue
    private Long id;
    private Date date;
    //private double time;
    private boolean done;
    //private boolean initialized;
    @ManyToOne
    private User user;
    @ManyToMany
    @JoinTable(name = "questions_simulated", joinColumns = @JoinColumn(name = "simulated_id"),
            inverseJoinColumns = @JoinColumn(name = "question_id"))
    @OrderBy("category ASC")
    private List<Question> questions = new LinkedList<>();

    public Simulated() {
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    /*public boolean isInitialized() {
        return initialized;
    }

    public void setInitialized(boolean initialized) {
        this.initialized = initialized;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }
    */

    public void setUser(User user) {
        this.user = user;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    @Override
    public String toString() {
        return "Simulated{" +
                "id=" + id +
                ", date=" + date +
                ", user=" + user +
                ", questions=" + questions.toString() +
                '}';
    }
}
