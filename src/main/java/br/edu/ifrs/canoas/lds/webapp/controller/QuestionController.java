package br.edu.ifrs.canoas.lds.webapp.controller;

import java.util.LinkedList;
import java.util.List;

import br.edu.ifrs.canoas.lds.webapp.service.CategoryService;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.edu.ifrs.canoas.lds.webapp.config.auth.UserImpl;
import br.edu.ifrs.canoas.lds.webapp.domain.Alternative;
import br.edu.ifrs.canoas.lds.webapp.domain.Category;
import br.edu.ifrs.canoas.lds.webapp.domain.Question;
import br.edu.ifrs.canoas.lds.webapp.service.QuestionService;

@Controller
@Secured({"ROLE_TEACHER", "ROLE_ADMIN"})
@RequestMapping("/question/")
public class QuestionController {

    private final QuestionService questionService;
    private final CategoryService categoryService;
    private boolean error = false;

    public QuestionController(QuestionService questionService, CategoryService categoryService) {
        this.questionService = questionService;
        this.categoryService = categoryService;
    }

    @GetMapping("form")
    public ModelAndView newQuestion(Model model) {
        Question question = new Question();
        List<Alternative> alternatives = new LinkedList<>();
        for (int i = 0; i < 5; i++) {
            alternatives.add(new Alternative());
        }
        question.setAlternatives(alternatives);
        model.addAttribute("question", question);
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("action", "new");
        model.addAttribute("error", error);
        error = false;
        return new ModelAndView("/question/form");
    }

    @GetMapping("view/{id}")
    public ModelAndView viewQuestion(Model model, @PathVariable Long id) {
        model.addAttribute("question", questionService.getOne(id));
        model.addAttribute("action", "view");
        return new ModelAndView("/question/form");
    }

    @GetMapping("edit/{id}")
    public ModelAndView editQuestion(Model model, @PathVariable Long id) {
        Question question = questionService.getOne(id);
        if (question.getAlternatives() != null) {
            for (Alternative alternative : question.getAlternatives()) {
                if (alternative.isCorrect()) {
                    question.setCorrect(Integer.toString(question.getAlternatives().indexOf(alternative)));
                }
            }
            int size = question.getAlternatives().size();
            for (int i = size; i < 5; i++) {
                question.getAlternatives().add(new Alternative());
            }
        }
        model.addAttribute("question", question);
        model.addAttribute("categories", categoryService.findAll());
        model.addAttribute("action", "edit");
        return new ModelAndView("/question/form");
    }

    @GetMapping("disable/{id}")
    public String disableQuestion(@PathVariable Long id) {
        questionService.disable(id);
        return "redirect:/question/list";
    }

    @GetMapping("enable/{id}")
    public String enableQuestion(@PathVariable Long id) {
        questionService.enable(id);
        return "redirect:/question/list";
    }

    @GetMapping("list")
    public ModelAndView listQuestions(Model model) {
        model.addAttribute("questions", questionService.findAll());
        return new ModelAndView("/question/list");
    }

    @PostMapping("save")
    public String save(@ModelAttribute("question") Question question, BindingResult bindingResult,
                       RedirectAttributes redirectAttr, @AuthenticationPrincipal UserImpl activeUser, Errors errors, Model model) {
        if (question.getAlternatives().size() != 0 && !question.getContent().isEmpty() && question.getCategory() != null) {
            System.out.println("QUESTION OK");
            if (question.getCorrect() != null && !question.getCorrect().isEmpty()) {
                question.getAlternatives().get(Integer.parseInt(question.getCorrect())).setCorrect(true);
                questionService.save(question);
            }
        } else {
            error = true;
            System.out.println("ERROR TRUE");
            return ("redirect:/question/form");
        }
        return ("redirect:/question/list");
    }
}
