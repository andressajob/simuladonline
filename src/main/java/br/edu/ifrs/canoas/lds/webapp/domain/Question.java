package br.edu.ifrs.canoas.lds.webapp.domain;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Question implements Comparable<Question> {

    @Id
    @GeneratedValue
    private Long id;
    @Size(max = 2000)
    private String content;
    private boolean active;
    @OneToMany(mappedBy = "question")
    private List<Alternative> alternatives;
    @ManyToOne
    private Category category;
    @Transient
    private String correct;
    @Transient
    private String choosed;


    public Question() {
        super();
        alternatives = new LinkedList<Alternative>();
        active = true;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<Alternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    public String getChoosed() {
        return choosed;
    }

    public void setChoosed(String choosed) {
        this.choosed = choosed;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", content='" + content + '}';
    }

    @Override
    public int compareTo(Question question) {
        String category = question.getCategory().getContent().toUpperCase();
        String category2 = question.getCategory().getContent().toUpperCase();
        return category.compareTo(category2);
    }

    public static Comparator<Question> QuestionCategoryComparator
            = new Comparator<Question>() {
        public int compare(Question question1, Question question2) {
            String fruitName1 = question1.getCategory().getContent().toUpperCase();
            String fruitName2 = question2.getCategory().getContent().toUpperCase();
            return fruitName1.compareTo(fruitName2);

        }

    };

}
