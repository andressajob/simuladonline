package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.*;
import br.edu.ifrs.canoas.lds.webapp.domain.utils.QuestionsSimulated;
import br.edu.ifrs.canoas.lds.webapp.repository.SimulatedRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SimulatedService {
    private final SimulatedRepository simulatedRepository;
    private final QuestionService questionService;
    private final UserService userService;

    public SimulatedService(SimulatedRepository simulatedRepository, QuestionService questionService, UserService userService) {
        this.simulatedRepository = simulatedRepository;
        this.questionService = questionService;
        this.userService = userService;
    }

    public Simulated generateSimulated(QuestionsSimulated simulated, User user) {
        LinkedList<Question> questions = new LinkedList<>();
        LinkedList<Question> questionFromRepository;
        Question question;
        if (user != null && simulated != null) {
            if (simulated.getAmountSpecifics() > 0) {
                questionFromRepository = questionService.findAllByCategory_IdAndActiveIsTrue(1L);
                Collections.shuffle(questionFromRepository);
                if (simulated.getAmountSpecifics() > questionFromRepository.size()) {
                    simulated.setAmountSpecifics(questionFromRepository.size());
                }
                for (int i = 0; i < simulated.getAmountSpecifics(); i++) {
                    question = questionFromRepository.get(i);
                    if (!questions.contains(question)) {
                        questions.add(question);
                    }
                }
            }
            if (simulated.getAmountGeneral() > 0) {
                questionFromRepository = questionService.findAllByCategory_IdAndActiveIsTrue(2L);
                Collections.shuffle(questionFromRepository);
                if (simulated.getAmountGeneral() > questionFromRepository.size()) {
                    simulated.setAmountGeneral(questionFromRepository.size());
                }
                for (int i = 0; i < simulated.getAmountGeneral(); i++) {
                    question = questionFromRepository.get(i);
                    if (!questions.contains(question)) {
                        questions.add(question);
                    }
                }
            }
            if (simulated.getAmountPortuguese() > 0) {
                questionFromRepository = questionService.findAllByCategory_IdAndActiveIsTrue(6L);
                Collections.shuffle(questionFromRepository);
                if (simulated.getAmountPortuguese() > questionFromRepository.size()) {
                    simulated.setAmountPortuguese(questionFromRepository.size());
                }
                for (int i = 0; i < simulated.getAmountPortuguese(); i++) {
                    question = questionFromRepository.get(i);
                    if (!questions.contains(question)) {
                        questions.add(question);
                    }
                }
            }
            if (simulated.getAmountComputing() > 0) {
                questionFromRepository = questionService.findAllByCategory_IdAndActiveIsTrue(3L);
                System.out.println(questionFromRepository.size());
                Collections.shuffle(questionFromRepository);
                if (simulated.getAmountComputing() > questionFromRepository.size()) {
                    simulated.setAmountComputing(questionFromRepository.size());
                }
                for (int i = 0; i < simulated.getAmountComputing(); i++) {
                    question = questionFromRepository.get(i);
                    if (!questions.contains(question)) {
                        questions.add(question);
                    }
                }
            }
            if (simulated.getAmountLegislation() > 0) {
                questionFromRepository = questionService.findAllByCategory_IdAndActiveIsTrue(4L);
                Collections.shuffle(questionFromRepository);
                if (simulated.getAmountLegislation() > questionFromRepository.size()) {
                    simulated.setAmountLegislation(questionFromRepository.size());
                }
                for (int i = 0; i < simulated.getAmountLegislation(); i++) {
                    question = questionFromRepository.get(i);
                    if (!questions.contains(question)) {
                        questions.add(question);
                    }
                }
            }
            if (simulated.getAmountMathematics() > 0) {
                questionFromRepository = questionService.findAllByCategory_IdAndActiveIsTrue(5L);
                Collections.shuffle(questionFromRepository);
                if (simulated.getAmountMathematics() > questionFromRepository.size()) {
                    simulated.setAmountMathematics(questionFromRepository.size());
                }
                for (int i = 0; i < simulated.getAmountMathematics(); i++) {
                    question = questionFromRepository.get(i);
                    if (!questions.contains(question)) {
                        questions.add(question);
                    }
                }
            }
            if (questions.size() > 0) {
                simulated.getSimulated().setDate(new Date());
                simulated.getSimulated().setQuestions(questions);
                simulated.getSimulated().setUser(user);
                simulated.getSimulated().setDone(false);
            /*simulated.getSimulated().setInitialized(false);
            simulated.getSimulated().setTime(5 * simulated.getSimulated().getQuestions().size());*/
                return simulatedRepository.save(simulated.getSimulated());
            }
        }
        return null;
    }

    public Simulated getOne(Long id) {
        if (id != null && simulatedRepository.existsById(id))
            return simulatedRepository.getOne(id);
        return null;
    }

    public Simulated doneSimulated(Long id) {
        if (id != null && simulatedRepository.existsById(id)) {
            Simulated simulated = simulatedRepository.getOne(id);
            simulated.setDone(true);
            return simulatedRepository.save(simulated);
        }
        return null;
    }

    public Simulated save(Simulated simulated) {
        if (simulated != null) {
            return simulatedRepository.save(simulated);
        }
        return null;
    }

    public boolean existsById(Long id) {
        if (id != null)
            return simulatedRepository.existsById(id);
        return false;
    }

    public List<Simulated> findAllByUserId(Long id) {
        if (id != null && userService.existsById(id))
            return simulatedRepository.findAllByUserId(id);
        return null;
    }

    public List<Simulated> findAllByUserIdAndDoneTrue(Long id) {
        if (id != null && userService.existsById(id))
            return simulatedRepository.findAllByUserIdAndDoneTrue(id);
        return null;
    }

    public List<Object> countHitsOnSimulated(Simulated simulated, List<Historic> historics) {
        List<Object> list = new LinkedList<>();
        if (historics != null && simulated != null)
            if (existsById(simulated.getId())) {
                Integer hits = 0;
                List<Alternative> corrects = new LinkedList<>();
                List<Alternative> chooseds = new LinkedList<>();
                for (Question question : simulated.getQuestions()) {
                    for (Historic historic : historics) {
                        if (historic.getQuestion().getId() == question.getId()) {
                            chooseds.add(historic.getAlternative());
                        }
                    }
                    for (Alternative alternative : question.getAlternatives()) {
                        if (alternative.isCorrect())
                            corrects.add(alternative);
                    }
                }
                for (int i = 0; i < simulated.getQuestions().size(); i++) {
                    if (chooseds.size() > 0) {
                        if (chooseds.get(i).getId() != null) {
                            if (chooseds.get(i).getId() == corrects.get(i).getId()) {
                                hits++;
                            }
                        }
                    }
                }
                list.add(chooseds);
                list.add(corrects);
                list.add(hits);
            }
        return list;
    }


}
