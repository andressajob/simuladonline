package br.edu.ifrs.canoas.lds.webapp.repository;

import br.edu.ifrs.canoas.lds.webapp.domain.Historic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoricRepository extends JpaRepository<Historic, Long> {
    public List<Historic> findAllBySimulatedId(long id);
    public List<Historic> findAllByUserId(long id);

}
