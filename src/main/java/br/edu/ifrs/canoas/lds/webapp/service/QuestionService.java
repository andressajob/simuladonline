package br.edu.ifrs.canoas.lds.webapp.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import br.edu.ifrs.canoas.lds.webapp.domain.Alternative;
import br.edu.ifrs.canoas.lds.webapp.domain.Question;
import br.edu.ifrs.canoas.lds.webapp.repository.QuestionRepository;

import javax.transaction.Transactional;

@Service
@Transactional
public class QuestionService {
    private final QuestionRepository questionRepository;
    private final AlternativeService alternativeService;
    private final CategoryService categoryService;

    public QuestionService(QuestionRepository questionRepository, AlternativeService alternativeService, CategoryService categoryService) {
        this.questionRepository = questionRepository;
        this.alternativeService = alternativeService;
        this.categoryService = categoryService;
    }

    public Question getOne(Long id) {
        if (id != null && questionRepository.existsById(id))
            return questionRepository.getOne(id);
        return null;
    }

    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    public Question save(Question question) {
        if (question != null) {
            if (question.getAlternatives() != null) {
                for (Alternative alternative : question.getAlternatives()) {
                    if (alternative.getContent() != null && alternative.getContent().length() > 0) {
                        alternative.setQuestion(question);
                        alternativeService.save(alternative);
                    }
                }
            }
            return questionRepository.save(question);
        }
        return null;
    }

    public boolean disable(Long id) {
        if (id != null && questionRepository.existsById(id)) {
            Question question = questionRepository.getOne(id);
            question.setActive(false);
            question = questionRepository.save(question);
            return !question.isActive();
        }
        return false;
    }

    public boolean enable(Long id) {
        if (id != null && questionRepository.existsById(id)) {
            Question question = questionRepository.getOne(id);
            question.setActive(true);
            question = questionRepository.save(question);
            return question.isActive();
        }
        return false;
    }

    public LinkedList<Question> findAllByCategory_IdAndActiveIsTrue(Long id) {
        if (id != null && categoryService.existsById(id))
            return questionRepository.findAllByCategory_IdAndActiveIsTrue(id);
        return null;
    }

}
