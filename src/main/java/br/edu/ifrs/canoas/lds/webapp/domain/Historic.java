package br.edu.ifrs.canoas.lds.webapp.domain;

import javax.persistence.*;

@Entity
public class Historic {

    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @ManyToOne
    @JoinColumn(name = "question_id")
    private Question question;
    @ManyToOne
    @JoinColumn(name = "simulated_id")
    private Simulated simulated;
    @ManyToOne
    @JoinColumn(name = "alternative_id")
    private Alternative alternative;


    public Historic() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public Simulated getSimulated() {
        return simulated;
    }

    public void setSimulated(Simulated simulated) {
        this.simulated = simulated;
    }

    public Alternative getAlternative() {
        return alternative;
    }

    public void setAlternative(Alternative alternative) {
        this.alternative = alternative;
    }
}
