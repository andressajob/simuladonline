package br.edu.ifrs.canoas.lds.webapp.repository;

import br.edu.ifrs.canoas.lds.webapp.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;
import java.util.List;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
   public LinkedList<Question> findAllByCategory_IdAndActiveIsTrue(Long id);

}
