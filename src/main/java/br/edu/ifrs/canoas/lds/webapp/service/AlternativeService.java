package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Alternative;
import br.edu.ifrs.canoas.lds.webapp.repository.AlternativeRepository;
import org.springframework.stereotype.Service;

@Service
public class AlternativeService {
    private final AlternativeRepository alternativeRepository;

    public AlternativeService(AlternativeRepository alternativeRepository) {
        this.alternativeRepository = alternativeRepository;
    }

    public Alternative getOne(Long id) {
        if (id != null && alternativeRepository.existsById(id))
            return alternativeRepository.getOne(id);
        return null;
    }

    public Alternative save(Alternative alternative) {
        if (alternative != null)
            return alternativeRepository.save(alternative);
        return null;
    }
}
