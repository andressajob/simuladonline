package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Category;
import br.edu.ifrs.canoas.lds.webapp.repository.CategoryRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Category getOne(Long id) {
        if (id != null && categoryRepository.existsById(id))
            return categoryRepository.getOne(id);
        return null;
    }

    public List<Category> findAll() {
        return categoryRepository.findAll();
    }

    public boolean existsById(Long id) {
        if (id != null)
            return categoryRepository.existsById(id);
        return false;
    }
}
