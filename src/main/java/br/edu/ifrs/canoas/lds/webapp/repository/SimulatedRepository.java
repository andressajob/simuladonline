package br.edu.ifrs.canoas.lds.webapp.repository;

import br.edu.ifrs.canoas.lds.webapp.domain.Question;
import br.edu.ifrs.canoas.lds.webapp.domain.Simulated;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SimulatedRepository extends JpaRepository<Simulated, Long> {
    List<Simulated> findAllByUserId(long id);
    List<Simulated> findAllByUserIdAndDoneTrue(long id);
}
