package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Alternative;
import br.edu.ifrs.canoas.lds.webapp.domain.Historic;
import br.edu.ifrs.canoas.lds.webapp.domain.Role;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.repository.UserRepository;

import java.util.HashSet;
import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;
    private final CategoryService categoryService;

    public UserService(UserRepository userRepository, RoleService roleService, CategoryService categoryService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
        this.categoryService = categoryService;
    }

    public User save(User user) {
        if (user != null) {
            if (userRepository.existsByUsername(user.getUsername().toLowerCase()) && user.getId() == null) {
                return null;
            } else {
                HashSet<Role> roles = new HashSet<>();
                PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                if (user.getRoles().isEmpty()) {
                    roles.add(roleService.getOne(1L));
                    user.setRoles(roles);
                }
                if (user.getId() != null) {
                    if (userRepository.getOne(user.getId()).isActive())
                        user.setActive(true);
                } else {
                    user.setActive(true);
                }
                String hashedPassword = passwordEncoder.encode(user.getPassword()); //encript password
                user.setPassword(hashedPassword);
                user.setUsername(user.getUsername().toLowerCase());
                return userRepository.save(user);
            }
        }
        return null;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    /*public User getOne(User user) {
        if (user != null)
            return userRepository.getOne(user.getId());
        return null;
    }*/

    public User getOne(Long id) {
        if (id != null && userRepository.existsById(id))
            return userRepository.getOne(id);
        return null;
    }

    public boolean disable(Long id) {
        if (id != null && userRepository.existsById(id)) {
            User user = userRepository.getOne(id);
            user.setActive(false);
            user = userRepository.save(user);
            return !user.isActive();
        }
        return false;
    }

    public boolean enable(Long id) {
        if (id != null && userRepository.existsById(id)) {
            User user = userRepository.getOne(id);
            user.setActive(true);
            user = userRepository.save(user);
            return user.isActive();
        }
        return false;
    }

    public int questionsDoneByCategory(Long idCategory, List<Historic> historics) {
        int done = -1;
        if (idCategory != null && categoryService.existsById(idCategory) && historics != null) {
            done = 0;
            for (Historic historic : historics) {
                if (historic.getQuestion().getCategory().getId() == (idCategory)) {
                    done++;
                }
            }
        }
        return done;
    }

    public int questionsCorrectByCategory(Long idCategory, List<Historic> historics) {
        int correct = -1;
        if (idCategory != null && categoryService.existsById(idCategory) && historics != null) {
            correct = 0;
            for (Historic historic : historics) {
                if (historic.getQuestion().getCategory().getId() == (idCategory)) {
                    for (Alternative alternative : historic.getQuestion().getAlternatives()) {
                        if (alternative.isCorrect() && historic.getAlternative() == alternative) {
                            correct++;
                        }
                    }
                }
            }
        }
        return correct;
    }

    public boolean existsById(Long id) {
        if (id != null)
            return userRepository.existsById(id);
        return false;
    }
}
