package br.edu.ifrs.canoas.lds.webapp.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class HistoricServiceTest {

    @Autowired
    private HistoricService historicService;

    @Test
    public void test_getOne_should_retrieve_historic_by_id() { assertThat(historicService.getOne(1001L)).isNotNull(); }

    @Test
    public void test_getOne_id_null(){
        assertThat(historicService.getOne(null)).isNull();
    }

    @Test
    public void test_getOne_id_invalid(){
        assertThat(historicService.getOne(-1L)).isNull();
    }

    @Test
    public void test_save_null_object(){
        assertThat(historicService.save(null)).isNull();
    }

    @Test
    public void test_findAllBySimulatedId_id_invalid(){
        assertThat(historicService.findAllBySimulatedId(-1L)).isNull();
    }

    @Test
    public void test_findAllBySimulatedId_id_null(){
        assertThat(historicService.findAllBySimulatedId(null)).isNull();
    }

    @Test
    public void test_countQuestionsDone_id_invalid(){
        assertThat(historicService.countQuestionsDone(-1L)).isNull();
    }

    @Test
    public void test_countQuestionsDone_id_null(){
        assertThat(historicService.countQuestionsDone(null)).isNull();
    }
}
