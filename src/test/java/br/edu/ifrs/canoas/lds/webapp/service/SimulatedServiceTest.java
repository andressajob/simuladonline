package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.*;
import br.edu.ifrs.canoas.lds.webapp.domain.utils.QuestionsSimulated;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class SimulatedServiceTest {

    private static final Long USER_ID = 1001L;
    private static final String USER_MAIL = "tony@stark.com";
    private static final String USER = "user";
    private static final String NAME = "Tony Stark";
    private static final String PASSWORD = "user";
    private static HashSet<Role> ROLE = new HashSet<>();

    @Autowired
    private SimulatedService simulatedService;

    @Before
    public void setUp() {
        ROLE.add(new Role("ROLE_USER"));
    }

    @Test
    public void test_getOne() {
        assertThat(simulatedService.getOne(1001L)).isNotNull();
    }

    @Test
    public void test_getOne_id_null() {
        assertThat(simulatedService.getOne(null)).isNull();
    }

    @Test
    public void test_getOne_id_invalid() {
        assertThat(simulatedService.getOne(-1L)).isNull();
    }

    @Test
    public void test_generateSimulated_user_and_simulated_null() {
        assertThat(simulatedService.generateSimulated(new QuestionsSimulated(), new User())).isNull();
    }

    @Test
    public void test_generateSimulated_user_and_simulated() {
        assertThat(simulatedService.generateSimulated(
                new QuestionsSimulated(1, 1, 1,
                        1, 1, 1, true, new Simulated()),
                this.getUser())).isNotNull();
    }

    @Test
    public void test_save_simulated() {
        assertThat(simulatedService.save(new Simulated())).isNotNull();
    }

    @Test
    public void test_save_simulated_null() {
        assertThat(simulatedService.save(null)).isNull();
    }

    @Test
    public void test_doneSimulated() {
        assertThat(simulatedService.doneSimulated(1002L)).isNotNull();
    }

    @Test
    public void test_doneSimulated_id_null() {
        assertThat(simulatedService.doneSimulated(null)).isNull();
    }

    @Test
    public void test_doneSimulated_id_invalid() {
        assertThat(simulatedService.doneSimulated(-1L)).isNull();
    }

    @Test
    public void test_findAllByUserId_id_null() {
        assertThat(simulatedService.findAllByUserId(null)).isNull();
    }

    @Test
    public void test_findAllByUserId_id_invalid() {
        assertThat(simulatedService.findAllByUserId(-1L)).isNull();
    }

    @Test
    public void test_findAllByUserIdAndDoneTrue_id_null() {
        assertThat(simulatedService.findAllByUserIdAndDoneTrue(null)).isNull();
    }

    @Test
    public void test_findAllByUserIdAndDoneTrue_id_invalid() {
        assertThat(simulatedService.findAllByUserIdAndDoneTrue(-1L)).isNull();
    }

    @Test
    public void test_countHitsOnSimulated_parameters_null() {
        assertThat(simulatedService.countHitsOnSimulated(null, null)).size().isEqualTo(0);
    }

    @Test
    public void test_existsById_id_invalid(){
        assertThat(simulatedService.existsById(-1L)).isFalse();
    }

    @Test
    public void test_existsById_id_null(){
        assertThat(simulatedService.existsById(null)).isFalse();
    }

    private User getUser(){
        User user = new User();
        user.setId(USER_ID);
        user.setUsername(USER);
        user.setEmail(USER_MAIL);
        user.setName(NAME);
        user.setActive(true);
        user.setPassword(PASSWORD);
        user.setRoles(ROLE);
        return user;
    }
}
