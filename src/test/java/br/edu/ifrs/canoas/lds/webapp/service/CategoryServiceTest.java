package br.edu.ifrs.canoas.lds.webapp.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class CategoryServiceTest {
    @Autowired
    private CategoryService categoryService;

    @Test
    public void test_getOne_should_retrieve_category_by_id() {
        assertThat(categoryService.getOne(1L)).isNotNull();
    }

    @Test
    public void test_getOne_id_null() {
        assertThat(categoryService.getOne(null)).isNull();
    }

    @Test
    public void test_getOne_id_invalid() {
        assertThat(categoryService.getOne(-1L)).isNull();
    }

    @Test
    public void test_findAll() {
        assertThat(categoryService.findAll()).size().isEqualTo(6);
    }

    @Test
    public void test_existsById_id_invalid(){
        assertThat(categoryService.existsById(-1L)).isFalse();
    }

    @Test
    public void test_existsById_id_null(){
        assertThat(categoryService.existsById(null)).isFalse();
    }

}
