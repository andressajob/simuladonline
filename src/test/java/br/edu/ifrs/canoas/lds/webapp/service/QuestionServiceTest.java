package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Alternative;
import br.edu.ifrs.canoas.lds.webapp.domain.Category;
import br.edu.ifrs.canoas.lds.webapp.domain.Question;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class QuestionServiceTest {

    private static final long QUESTION_ID = 1001L;
    private static final String CONTENT = "Qual a tecla de atalho utilizada para localizar um texto em uma página exibida no Windows Internet Explorer?";
    private final LinkedList<Alternative> ALTERNATIVES = new LinkedList<>
            (Arrays.asList(new Alternative("CTRL+B", false, this.getQuestion()),
                    new Alternative("CTRL+F", true, this.getQuestion()),
                    new Alternative("CTRL+P", false, this.getQuestion()),
                    new Alternative("CTRL+T", false, this.getQuestion())));

    @Autowired
    private QuestionService questionService;

    @Test
    public void test_getOne_should_retrieve_question_by_id() {
        assertThat(questionService.getOne(this.getQuestion().getId())).isNotNull();
    }

    @Test
    public void test_getOne_id_null(){
        assertThat(questionService.getOne(null)).isNull();
    }

    @Test
    public void test_getOne_id_invalid(){
        assertThat(questionService.getOne(-1L)).isNull();
    }
    @Test
    public void test_findAll(){
        assertThat(questionService.findAll().size()).isEqualTo(60);
    }

    @Test
    public void test_save(){
        assertThat(questionService.save(this.getQuestion())).isNotNull();
    }

    @Test
    public void test_save_null_object(){
        assertThat(questionService.save(null)).isNull();
    }

    @Test
    public void test_disable_id_null(){
        assertThat(questionService.disable(null)).isFalse();
    }

    @Test
    public void test_enable_id_null(){
        assertThat(questionService.enable(null)).isFalse();
    }

    @Test
    public void test_disable_id_invalid(){
        assertThat(questionService.disable(-1L)).isFalse();
    }

    @Test
    public void test_enable_id_invalid(){
        assertThat(questionService.enable(-1L)).isFalse();
    }

    private Question getQuestion(){
        Question question = new Question();
        question.setId(QUESTION_ID);
        question.setContent(CONTENT);
        question.setAlternatives(ALTERNATIVES);
        question.setActive(true);
        question.setCategory(new Category("Informática"));
        return question;
    }

}
