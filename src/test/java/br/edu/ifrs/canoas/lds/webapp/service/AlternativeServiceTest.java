package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Alternative;
import br.edu.ifrs.canoas.lds.webapp.domain.Question;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class AlternativeServiceTest {

    @Autowired
    private AlternativeService alternativeService;

    @Test
    public void test_getOne() {
        assertThat(alternativeService.getOne(1001L)).isNotNull();
    }

    @Test
    public void test_getOne_id_null() {
        assertThat(alternativeService.getOne(null)).isNull();
    }

    @Test
    public void test_getOne_id_invalid() {
        assertThat(alternativeService.getOne(-1L)).isNull();
    }

    @Test
    public void test_save_alternative_null() {
        assertThat(alternativeService.save(null)).isNull();
    }

    @Test
    public void test_save_alternative() {
        assertThat(alternativeService.save(this.getAlternative())).isNotNull();
    }

    private Alternative getAlternative(){
        Alternative alternative = new Alternative();
        alternative.setCorrect(true);
        alternative.setContent("a reposta certa.");
        alternative.setQuestion(new Question());
        return alternative;
    }
}
