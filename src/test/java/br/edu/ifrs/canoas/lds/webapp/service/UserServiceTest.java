package br.edu.ifrs.canoas.lds.webapp.service;

import br.edu.ifrs.canoas.lds.webapp.domain.Role;
import br.edu.ifrs.canoas.lds.webapp.domain.User;
import br.edu.ifrs.canoas.lds.webapp.repository.HistoricRepository;
import br.edu.ifrs.canoas.lds.webapp.repository.RoleRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.LinkedList;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
public class UserServiceTest {

    private static final Long USER_ID = 1001L;
    private static final String USER_MAIL = "thor@asgard.com";
    private static final String USER = "thor";
    private static final String NAME = "Thor Odinson";
    private static final String PASSWORD = "thor";
    private static HashSet<Role> ROLE = new HashSet<>();

    @Autowired
    private UserService userService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private HistoricRepository historicRepository;

    @Before
    public void setUp() {
        ROLE.add(roleRepository.getOne(1L));
    }

    @Test
    public void the_getOne_should_retrieve_user_by_id() {
        assertThat(userService.getOne(USER_ID).getUsername()).isEqualTo("user");
        assertThat(userService.getOne(USER_ID).getEmail()).isEqualTo("tony@stark.com");
        assertThat(userService.getOne(USER_ID).getName()).isEqualTo("Tony Stark");
    }

    @Test
    public void test_getOne_id_null() {
        assertThat(userService.getOne(null)).isNull();
    }

    @Test
    public void test_getOne_id_invalid() {
        assertThat(userService.getOne(-1L)).isNull();
    }

    @Test
    public void test_findAll() {
        assertThat(userService.findAll().size()).isEqualTo(3);
    }

    @Test
    public void test_save() {
        assertThat(userService.save(new User(NAME, USER, USER_MAIL, PASSWORD, true, ROLE))).isNotNull();
    }

    @Test
    public void test_save_user_username_exists() {
        assertThat(userService.save(new User(NAME, "user", USER_MAIL, PASSWORD, true, ROLE))).isNull();
    }

    @Test
    public void test_save_user_id_exists() {
        assertThat(userService.save(userService.getOne(1002L))).isNotNull();
    }

    @Test
    public void test_save_user_roles_empty() {
        assertThat(userService.save(new User(NAME, "dessa", USER_MAIL, PASSWORD, true, new HashSet<>()))).isNotNull();
    }

    @Test
    public void test_save_null_object() {
        assertThat(userService.save(null)).isNull();
    }

    @Test
    public void test_disable_id_valid() {
        assertThat(userService.disable(1001L)).isTrue();
    }

    @Test
    public void test_enable_id_valid() {
        assertThat(userService.enable(1002L)).isTrue();
    }

    @Test
    public void test_disable_id_null() {
        assertThat(userService.disable(null)).isFalse();
    }

    @Test
    public void test_enable_id_null() {
        assertThat(userService.enable(null)).isFalse();
    }

    @Test
    public void test_disable_id_invalid() {
        assertThat(userService.disable(-1L)).isFalse();
    }

    @Test
    public void test_enable_id_invalid() {
        assertThat(userService.enable(-1L)).isFalse();
    }

    @Test
    public void test_questionsCorrectByCategory_id_and_historics_null() {
        assertThat(userService.questionsCorrectByCategory(null, null)).isEqualTo(-1);
    }

    @Test
    public void test_questionsCorrectByCategory_id_and_historics_invalids() {
        assertThat(userService.questionsCorrectByCategory(-1L, new LinkedList<>())).isEqualTo(-1);
    }

    @Test
    public void test_questionsDoneByCategory_id_and_historics_null() {
        assertThat(userService.questionsDoneByCategory(null, null)).isEqualTo(-1);
    }

    @Test
    public void test_questionsDoneByCategory_id_and_historics_invalids() {
        assertThat(userService.questionsDoneByCategory(-1L, new LinkedList<>())).isEqualTo(-1);
    }

    @Test
    public void test_questionsCorrectByCategory_id_and_historics() {
        assertThat(userService.questionsCorrectByCategory(1L, historicRepository.findAllByUserId(1001L))).isGreaterThanOrEqualTo(0);
    }

    @Test
    public void test_questionsDoneByCategory_id_and_historics() {
        assertThat(userService.questionsCorrectByCategory(1L, historicRepository.findAllByUserId(1001L))).isGreaterThanOrEqualTo(0);
    }

    @Test
    public void test_existsById_id_invalid() {
        assertThat(userService.existsById(-1L)).isFalse();
    }

    @Test
    public void test_existsById_id_null() {
        assertThat(userService.existsById(null)).isFalse();
    }

}